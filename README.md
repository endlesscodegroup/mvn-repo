# Popular Minecraft Plugins Maven Repository
This is Maven repository that contains most popular Bukkit plugins. That needed in development of EndlessCode projects.

Repo url: https://gitlab.com/endlesscodegroup/mvn-repo/raw/master/

#### Maven
```xml
<repositories>
  <repository>
    <id>ec-repo</id>
    <name>EndlessCode Repository</name>
    <url>https://gitlab.com/endlesscodegroup/mvn-repo/raw/master/</url>
  </repository>
</repositories>
```

#### Gradle
```groovy
repositories {
  maven {
    name = "ec-repo"
    url = "https://gitlab.com/endlesscodegroup/mvn-repo/raw/master/"
  }
}
```

### Plugins list:
- MCCore ([com.sucy:MCCore](https://github.com/EndlessCodeGroup/mvn-repo/tree/master/com/sucy/MCCore))
- SkillAPI ([com.sucy:SkillAPI](https://github.com/EndlessCodeGroup/mvn-repo/tree/master/com/sucy/SkillAPI))
- Heroes ([com.herocraftonline.heroes:Heroes](https://github.com/EndlessCodeGroup/mvn-repo/tree/master/com/herocraftonline/heroes/Heroes))
- BattleLevels-API ([me.robin.battlelevels:battlelevels-api](https://github.com/EndlessCodeGroup/mvn-repo/tree/master/me/robin/battlelevels/battlelevels-api))
- Inspector API ([ru.endlesscode.inspector:inspector-api](https://gitlab.com/endlesscodegroup/mvn-repo/tree/feature/inspector/ru/endlesscode/inspector/inspector-api/))
- Inspector Bukkit ([ru.endlesscode.inspector:inspector-bukkit](https://gitlab.com/endlesscodegroup/mvn-repo/tree/feature/inspector/ru/endlesscode/inspector/inspector-bukkit/))

## How to add plugin
There are two ways to do this:
  1. Create [Pull Request](https://github.com/EndlessCodeGroup/mvn-repo/pulls) with plugin
  2. Write request to [issues](https://github.com/EndlessCodeGroup/mvn-repo/issues)

## Others useful repos:
### lumine-repo
```groovy
maven {
    name 'lumine-repo'
    url 'http://dev.lumine.io:8081/repository/maven-snapshots/'
}
```
This repo contains xikage's plugins (MythicMobs)
